# [Security by Default: Development](https://wldhx.gitlab.io/security-by-default-development)

A talk on some more concrete / applied details of SbD. Examines OWASP Top 10, 'bit of crypto, most common low-level issues (buffer overflow / stack smashing, namely), some administration / deployment best practices. Also includes a healthy bit of truisms and first principles.

## Building

To build a local version of this deck, use

```sh
git clone https://gitlab.com/wldhx/security-by-default-development.git
cd security-by-default-development
yarn install
yarn run start # to start a local dev server
yarn run build # to build a static version
```
