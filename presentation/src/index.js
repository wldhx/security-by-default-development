import React from 'react';

import {
  Appear, CodePane, Deck, Fill,
  Heading, Link, ListItem, List, Slide,
  Spectacle,
} from 'spectacle';

import createTheme from 'spectacle/lib/themes/default';

require('normalize.css');
require('spectacle/lib/themes/default/index.css');

const theme = createTheme({
  primary: 'rgb(60, 60, 60)',
  secondary: 'hotpink',
  tertiary: 'white',
  quartenary: '#8BC34A',
});
theme.screen.components.list.listStylePosition = 'outside';
theme.screen.components.list.marginRight = '40px';
theme.screen.components.codePane.pre.margin = '20px auto';
theme.screen.components.codePane.pre.fontSize = '1.1rem';
theme.screen.components.heading.h3.color = '#8BC34A'; // FIXME: quartenary

export default () =>
  <Spectacle theme={theme}>
    <Deck transition={['zoom', 'slide']} transitionDuration={500} progress="bar">
      <Slide>
        <Heading size={1} fit>Security</Heading>
        <Heading size={2} fit>by default</Heading>
        <Heading size={3}>Development</Heading>
      </Slide>
      <Slide>
        <Link href="https://habrahabr.ru/post/325382"><Heading size={1}>Some truisms</Heading></Link>
        <List>
          <ListItem>If hacking it costs more than the profit it makes, it won't be hacked</ListItem>
          <ListItem>The more complex the system (deeper the tech stack), the more vulnerable it is</ListItem>
          <ListItem>Business only starts caring after the incident</ListItem>
          <ListItem>Reading docs is a good idea</ListItem>
          <ListItem>Humans go the low-friction path</ListItem>
          <ListItem>Humans are not secure</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading size={1}>Whole system matters</Heading>
        <List>
          <ListItem>Incident response</ListItem>
          <ListItem>Defense in depth</ListItem>
          <ListItem>"Weakest link in the chain", know your deps</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading size={1}>TOC</Heading>
        <List>
          <ListItem>Web</ListItem>
          <ListItem>Crypto</ListItem>
          <ListItem>Systems (low-level)</ListItem>
          <ListItem>Administration / deployment</ListItem>
          <ListItem>First principles</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading size={1} fit>Web</Heading>
        <Heading size={2}>OWASP</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Injection</Heading>
        <Heading size={3} textColor="primary">Use parametrized API</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Injection</Heading>
        <CodePane
          lang="sql"
          source={
`statement = "SELECT * FROM users WHERE name = '" + userName + "';"

where userName = ' OR '1'='1
=> SELECT * FROM users WHERE name = '' OR '1'='1';

where userName = a';DROP TABLE users; SELECT * FROM userinfo WHERE 't' = 't'
=> SELECT * FROM users WHERE name = 'a';DROP TABLE users; SELECT * FROM userinfo WHERE 't' = 't';

INSERT INTO PRODUCT (name, price) VALUES (?, ?)
`
          }
        />
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Broken Auth / Sessions</Heading>
        <Heading size={3} textColor="primary">Single source; simple dev interface; timeouts; encryption</Heading>
      </Slide>
      <Slide textColor="primary" bgColor="secondary">
        <Heading size={1}>Broken Auth / Sessions</Heading>
        <List>
          <ListItem>Support / account recovery procedures (Amazon)</ListItem>
          <ListItem>Tokens don't get invalidated</ListItem>
        </List>
        <CodePane
          source="http://example.com/sale/saleitems?sessionid=268544541&dest=Hawaii"
        />
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Cross-Site Scripting (XSS)</Heading>
        <Heading size={3} textColor="primary">Escape everything</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Insecure Direct Object References</Heading>
        <Heading size={3} textColor="primary">Use indirect refs / authorize</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Security Misconfiguration</Heading>
        <Heading size={3} textColor="primary">Updates, clean segregation, DiD</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Sensitive Data Exposure</Heading>
        <Heading size={3} textColor="primary">Encryption / hashing / probabilistic ds</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Missing Function Level Access Control</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Cross-Site Request Forgery (CSRF)</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Using Components with Known Vulnerabilities</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Unvalidated Redirects and Forwards</Heading>
      </Slide>
      <Slide>
        <Heading size={1} fit>Crypto</Heading>
      </Slide>
      <Slide>
        <Heading size={1}>Why?</Heading>
        <List>
          <ListItem>Prevent third parties from reading or modifying private messages</ListItem>
          <ListItem>Confidentiality, integrity, authentication, non-repudiation</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading size={1} fit>Systems</Heading>
        <Heading size={2}>MISRA-C / C++ Core Guidelines / Rust book</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Buffer overflow</Heading>
        <Heading size={3} textColor="primary">Static enforcement / runtime bounds check</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Appear>
          <CodePane
            lang="c"
            source={
`char           A[8] = "";
unsigned short B    = 1979;

strcpy(A, "too_much_bytes");`
            }
          />
        </Appear>
        <Appear>
          <CodePane
            lang="c"
            source={
`#include <string.h>

void foo (char *bar)
{
   char  c[12];

   strcpy(c, bar);  // no bounds checking
}

int main (int argc, char **argv)
{
   foo(argv[1]);

   return 0;
}`
          }
          />
        </Appear>
      </Slide>
      <Slide bgColor="secondary">
        <Heading size={1}>Race conditions</Heading>
        <Heading size={3} textColor="primary">Static enforcement (Send/Copy types)</Heading>
      </Slide>
      <Slide bgColor="secondary">
        <Appear>
          <CodePane
            lang="c++"
            source={
`if (val < 5) {
    // ... other thread can change val here ...
    switch (val) {
    case 0: // ...
    case 1: // ...
    case 2: // ...
    case 3: // ...
    case 4: // ...
    }
}`
            }
          />
        </Appear>
        <Appear>
          <CodePane
            lang="c++"
            source={
`void f(fstream&  fs, regex pat)
{
    array<double, max> buf;
    int sz = read_vec(fs, buf, max);            // read from fs into buf
    gsl::span<double> s {buf};
    // ...
    auto h1 = async([&]{ sort(par, s); });     // spawn a task to sort
    // ...
    auto h2 = async([&]{ return find_all(buf, sz, pat); });   // span a task to find matches
    // ...
}`
            }
          />
        </Appear>
      </Slide>
      <Slide>
        <CodePane
          lang="rust"
          source={
`fn use_vec() {
    let vec = make_vec();  // take ownership of the vector
    print_vec(vec);        // pass ownership to \`print_vec\`

    for i in vec.iter() {  // continue using \`vec\`
        println!("{}", i * 2)
    }
}`
          }
        />
        <Appear>
          <CodePane
            source={
`error: use of moved value: \`vec\`

for i in vec.iter() {
         ^~~`
            }
          />
        </Appear>
      </Slide>
      <Slide>
        <Heading size={1} fit>Administration</Heading>
        <Heading size={2}>setenforce 1</Heading>
      </Slide>
      <Slide>
        <Heading size={1} fit>Leave the safety on!</Heading>
        <CodePane
          lang="sh"
          source={
`$ setenforce 1
$ systemctl start firewalld`
          }
        />
        <Heading size={2}>RBAC and firewall are your friends</Heading>
      </Slide>
      <Slide>
        <Heading size={1} fit>Investigate your system</Heading>
        <Heading size={3}>First thing: updates</Heading>
        <CodePane
          lang="bash"
          source={
`$ dnf upgrade
$ dnf install dnf-automatic
$ $EDITOR /etc/dnf/automatic.conf
$ systemctl enable dnf-automatic.timer && systemctl start dnf-automatic.timer`
          }
        />
        <Heading size={3}>Second thing: open ports</Heading>
        <CodePane
          lang="bash"
          source={
`$ ss -tulpn # from inside
$ nmap $IP # from outside`
          }
        />
      </Slide>
      <Slide>
        <Heading size={1} fit>Configuration management evolution</Heading>
        <Appear>
          <Fill>
            <Heading size={4} bgColor="white" margin={10}>ad-hoc</Heading>
          </Fill>
        </Appear>
        <Appear>
          <Fill>
            <Heading size={4} bgColor="white" margin={10}>cookbooks</Heading>
          </Fill>
        </Appear>
        <Appear>
          <Fill>
            <Heading size={4} bgColor="white" margin={10}>immutable</Heading>
          </Fill>
        </Appear>
      </Slide>
      <Slide>
        <Heading size={1}>First principles</Heading>
        <List>
          <ListItem>Make your systems simple, static, immutable - understandable</ListItem>
          <ListItem>Minimise attack surface</ListItem>
          <ListItem>Encrypt communication, auth clients, rate-limit APIs</ListItem>
          <ListItem>Clean segregated interfaces</ListItem>
          <ListItem>Good tooling</ListItem>
        </List>
      </Slide>
      <Slide>
        <Heading fit size={1}>Questions?</Heading>
        <Heading fit size={3} textColor="quartenary">Shall anyone need me,</Heading>
        <Link href="mailto:wldhx+sbd2@wldhx.me"><Heading fit size={2} textColor="secondary">wldhx+sbd2@wldhx.me</Heading></Link>
        <Heading fit size={6} textColor="secondary">FB52 7CDA C117 6535 A4CF 9B4C 0E8C B6EC 231E DDFE</Heading>
      </Slide>
    </Deck>
  </Spectacle>;
